from PySide2.QtUiTools import QUiLoader
from PySide2 import QtWidgets

import sys

class MyWindow(QtWidgets.QMainWindow):
    def load(self):
        self.setWindowTitle("Python CI/CD")
        loader = QUiLoader()
        self.ui = loader.load('UI.ui')
        self.setCentralWidget(self.ui)


print("Hello World")
app = QtWidgets.QApplication(sys.argv)
window = MyWindow()
window.load()
window.resize(1280, 720)
window.show()
app.exec_()
